# -*- coding: utf-8 -*-
"""
Created on Tue Apr 18 14:20:52 2023

@author: 33753
"""


import unittest
from circle import circle_radius, NegativeRadius


class TestCircleArea(unittest.TestCase):

    def test_values(self):
        # Test if error is raised in case of incorrect value
        for value in [-1, -2, -4]:
            self.assertRaises(NegativeRadius, circle_radius, value)


if __name__ == '__main__':

    unittest.main()
