# -*- coding: utf-8 -*-
"""
Created on Tue Apr 18 14:29:46 2023

@author: 33753
"""

from math import pi


class NegativeRadius(Exception):
    def __init__(self, message="Radius cannot be negative"):
        self.message = "Radius cannot be negative"
        super().__init__(self.message)


def circle_radius(r):
    if r < 0:
        raise NegativeRadius
    return pi*(r**2)
